package ubb.electivo.proyecto.dto;

import org.springframework.data.rest.core.config.Projection;

import ubb.electivo.proyecto.entidades.Usuario;

@Projection(
		  name = "usuarioProjection", 
		  types = { Usuario.class }) 
public interface UsuarioProyection {
	
	String getNombreUsuario();
	String getEmailUsuario();
	String getNombreTipoUsuario();
}
