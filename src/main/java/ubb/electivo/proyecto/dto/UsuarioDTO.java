package ubb.electivo.proyecto.dto;


public class UsuarioDTO {
	
	private String nombreUsuario;
	private String emailusuario;
	private String nombreTipoUsuario;
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getEmailusuario() {
		return emailusuario;
	}
	public void setEmailusuario(String emailusuario) {
		this.emailusuario = emailusuario;
	}
	public String getNombreTipoUsuario() {
		return nombreTipoUsuario;
	}
	public void setNombreTipoUsuario(String nombreTipoUsuario) {
		this.nombreTipoUsuario = nombreTipoUsuario;
	}
	public UsuarioDTO(String nombreUsuario, String emailusuario, String nombreTipoUsuario) {
		super();
		this.nombreUsuario = nombreUsuario;
		this.emailusuario = emailusuario;
		this.nombreTipoUsuario = nombreTipoUsuario;
	}
	@Override
	public String toString() {
		return "UsuarioDTO [nombreUsuario=" + nombreUsuario + ", emailusuario=" + emailusuario + ", nombreTipoUsuario="
				+ nombreTipoUsuario + "]";
	}
	
	
	
}
