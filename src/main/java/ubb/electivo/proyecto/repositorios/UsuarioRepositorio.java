package ubb.electivo.proyecto.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ubb.electivo.proyecto.dto.UsuarioDTO;
import ubb.electivo.proyecto.dto.UsuarioProyection;
import ubb.electivo.proyecto.entidades.Usuario;

@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario, Long>{	
	
	//Metodos JPA
	List<Usuario> findUsuarioByActivo(Integer activo);
	
	//Query's JPA
	@Query( "SELECT "
			+ "new ubb.electivo.proyecto.dto.UsuarioDTO(u.nombreUsuario, u.emailUsuario, tu.nombreTipoUsuario) "
			+ "FROM Usuario u JOIN u.tipoUsuario tu ")
	List<UsuarioDTO> traerUsuariosPersonalizados();
	
	//Query's nativas
	@Query( value = "SELECT u.nombre_usuario nombreUsuario, u.email emailUsuario, tu.nombre_tipo_usuario nombreTipoUsuario "
			      + "FROM "
				  + "usuario.usuario u "
		          + "INNER JOIN usuario.tipo_usuario tu ON u.tipo_usuario_id = tu.id", nativeQuery = true)
	List<UsuarioProyection> traerUsuariosNativos();
	
}
