package ubb.electivo.proyecto.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class MetodosHttpService {
	
	HashMap<String, Object> datos;
	List<HashMap<String, Object>> listaDatos;
	
	public MetodosHttpService() {
		this.listaDatos = new LinkedList<HashMap<String,Object>>();
		this.datos = new HashMap<String, Object>();
		
		this.datos.put("nombre", "Ricardo Muñoz");
		this.datos.put("email", "ricardo@gmail.com");
		this.datos.put("rut", "18978599");
		
		this.listaDatos.add(this.datos);
		this.datos = new HashMap<String, Object>();
		
		this.datos.put("nombre", "Jorge Miguel");
		this.datos.put("email", "miguel@gmail.com");
		this.datos.put("rut", "19898765");
		
		
		this.listaDatos.add(this.datos);
		this.datos = new HashMap<String, Object>();
		
		this.datos.put("nombre", "Maria Jesu");
		this.datos.put("email", "maria@gmail.com");
		this.datos.put("rut", "19234123");
		
		this.listaDatos.add(this.datos);
		this.datos = new HashMap<String, Object>();
		
		
	}
	
	public List<HashMap<String, Object>> listarDatos(){
		return this.listaDatos;
	}
	
	public Object insertarDatos(HashMap<String, Object> datosEntrada){
		return (!datosEntrada.isEmpty())? this.listaDatos.add(datosEntrada): null;
	}
	
	public Object editarDatos(HashMap<String, Object> datosEntrada){
		for(HashMap<String, Object> index: this.listaDatos) {
			
			if(index.get("rut").toString().equals(datosEntrada.get("rut").toString())) {
				index.replace("nombre", datosEntrada.get("nombre"));
				index.replace("rut", datosEntrada.get("rut"));
				index.replace("email", datosEntrada.get("email"));
				return true;
			}else {
				this.datos.put("error", "no hay datos que editar");
				return this.datos;
			}
		}
		return datosEntrada;
	}
	
	public Object eliminarDatoPorRut(String rut){
		for(int i = 0; i<this.listaDatos.size();i++) {
			try {
				if(this.listaDatos.get(i).get("rut").equals(rut.toString())) {
					this.listaDatos.remove(i);
					return true;
				}
			} catch (Exception e) {
				return this.datos.put("error", "no hay datos que editar");
			}
			
		}
		return this.listaDatos;
	}
}
