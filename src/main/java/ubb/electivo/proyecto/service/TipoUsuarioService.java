package ubb.electivo.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ubb.electivo.proyecto.entidades.TipoUsuario;
import ubb.electivo.proyecto.repositorios.TipoUsuarioRepositorio;

@Service
public class TipoUsuarioService {
	
	@Autowired
	private TipoUsuarioRepositorio tipoUsuarioRepositorio;
	
	public List<TipoUsuario> traerDatos(){
		return this.tipoUsuarioRepositorio.findAll();
	}
	
	public Boolean crearTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuarioRepositorio.save(tipoUsuario);
		return true;
	}

}
