package ubb.electivo.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ubb.electivo.proyecto.dto.UsuarioDTO;
import ubb.electivo.proyecto.dto.UsuarioProyection;
import ubb.electivo.proyecto.entidades.Usuario;
import ubb.electivo.proyecto.repositorios.UsuarioRepositorio;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepositorio usuarioRepo;
	
	public List<Usuario> listarUsuario(){
		return this.usuarioRepo.findAll();
	}
	
	public List<Usuario> listarUsuarioMetodoJPA(Integer activo){
		return this.usuarioRepo.findUsuarioByActivo(activo);
	}
	
	public List<UsuarioDTO> listarUsuarioDto(){
		return this.usuarioRepo.traerUsuariosPersonalizados();
	}
	
	public List<UsuarioProyection> listarUsuariosNativos(){
		return this.usuarioRepo.traerUsuariosNativos();
	}
	
}
