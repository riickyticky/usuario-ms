package ubb.electivo.proyecto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ubb.electivo.proyecto.dto.UsuarioDTO;
import ubb.electivo.proyecto.dto.UsuarioProyection;
import ubb.electivo.proyecto.entidades.Usuario;
import ubb.electivo.proyecto.service.UsuarioService;

@RestController
@RequestMapping("/api")
public class UsuarioController {
		
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping(value = "listar-usuarios")
	public List<Usuario> listarTodosLosUsuarios(){
		return this.usuarioService.listarUsuario();
	}
	
	@GetMapping(value = "listar-usuarios-dto")
	public List<UsuarioDTO> listarTodosLosUsuariosDto(){
		return this.usuarioService.listarUsuarioDto();
	}
	
	@GetMapping(value = "listar-usuarios-nativos")
	public List<UsuarioProyection> listarTodosLosUsuariosNativos(){
		return this.usuarioService.listarUsuariosNativos();
	}
	
	@GetMapping(value = "listar-usuarios/{activo}")
	public List<Usuario> listarTodosLosUsuariosPorActivo(@RequestParam Integer activo){
		return this.usuarioService.listarUsuarioMetodoJPA(activo);
	}
}
