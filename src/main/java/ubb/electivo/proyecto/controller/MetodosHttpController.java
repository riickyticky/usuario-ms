package ubb.electivo.proyecto.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ubb.electivo.proyecto.service.MetodosHttpService;

@RestController
@RequestMapping("/api")
public class MetodosHttpController {
	
	@Autowired
	private MetodosHttpService metodosService;
	
	//@GetMapping(value = "/hola-mundo")
	public String listar() {
		return "hola-mundo";
	}
	
	@GetMapping(value = "/listar-datos")
	public List<HashMap<String, Object>> listarDatos() {
		return this.metodosService.listarDatos();
	}
	
	@PostMapping(value = "/insertar-datos", produces = {MediaType.APPLICATION_JSON_VALUE})
	public Object insertarDatos(@RequestBody HashMap<String, Object> datosEntrada) {
		return this.metodosService.insertarDatos(datosEntrada);
	}
	
	@PutMapping(value = "/editar-datos", produces = {MediaType.APPLICATION_JSON_VALUE})
	public Object editarDatos(@RequestBody HashMap<String, Object> datosEntrada) {
		return this.metodosService.editarDatos(datosEntrada);
	}
	
	@DeleteMapping(value = "/eliminar-dato-por-rut/{rut}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public Object editarDatos(@PathVariable String rut) {
		return this.metodosService.eliminarDatoPorRut(rut);
	}
}
