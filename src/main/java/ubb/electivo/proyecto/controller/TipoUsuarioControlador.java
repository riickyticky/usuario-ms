package ubb.electivo.proyecto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ubb.electivo.proyecto.entidades.TipoUsuario;
import ubb.electivo.proyecto.service.TipoUsuarioService;

@RestController
@RequestMapping("/api")
public class TipoUsuarioControlador {
	
	@Autowired
	private TipoUsuarioService tiposUsuarioService;
	
	@GetMapping(value = "leer-tipo-usuarios")
	public List<TipoUsuario> devolverListaTipoUsuario(){
		return this.tiposUsuarioService.traerDatos();
	}
	
	@PostMapping(value = "crear-tipo-usuario")
	public Boolean crearTipoUsuario(@RequestBody TipoUsuario tipoUsuario) {
		return this.tiposUsuarioService.crearTipoUsuario(tipoUsuario);
	}
}
